﻿using System;
using System.Collections.Generic;

namespace Lesson6.Domain
{
    /// <summary>
    /// Пользователь
    /// </summary>
    public class Customer
    {
        /// <summary>
        /// Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Имя
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Отчество
        /// </summary>
        public string Patronomic { get; set; }

        /// <summary>
        /// Фамилия
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Полное имя пользователя
        /// </summary>
        public string FullName => $"{LastName} {FirstName} {Patronomic}".Trim();

        /// <summary>
        /// Телефон
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Паспортные данные
        /// </summary>
        public string Passport { get; set; }

        /// <summary>
        /// Дата регистрации
        /// </summary>
        public DateTime RegisterDate { get; set; }

        /// <summary>
        /// Логин
        /// </summary>
        public string Login { get; set; }

        /// <summary>
        /// Пароль
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Счета пользователя
        /// </summary>
        public List<Account> Accounts { get; } = new List<Account>();

        public override string ToString() => $"Пользователь: {LastName} {FirstName} {Patronomic}\nТелефон: {Phone}\nПаспорт: {Passport}\nДата регистрации: {RegisterDate.ToShortDateString()}";
    }
}
