﻿using System;
using System.Collections.Generic;

namespace Lesson6.Domain
{
    /// <summary>
    /// Счёт клиента
    /// </summary>
    public class Account
    {
        /// <summary>
        /// Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Дата открытия счёта
        /// </summary>
        public DateTime CreationDate { get; set; }

        /// <summary>
        /// Сумма на счёте
        /// </summary>
        public decimal Balance { get; set; }

        #region связь с пользователем

        /// <summary>
        /// ID пользователя
        /// </summary>
        public int CustomerId { get; set; }

        /// <summary>
        /// Пользователь
        /// </summary>
        public Customer Customer { get; set; }

        #endregion

        #region связь с историей операций

        /// <summary>
        /// Список действий со счётом
        /// </summary>
        public List<History> Histories { get; } = new List<History>();

        #endregion

        public override string ToString() =>
            $"счёт клиента {Customer?.FullName} от {CreationDate.ToShortDateString()} c текущим балансом {Balance}";
    }
}
