﻿using System;
using System.Text;

namespace Lesson6.Domain
{
    public static class EntitiesExtensions
    {
        /// <summary>
        /// Строковое представление о счёте вместе с историей
        /// </summary>
        /// <param name="account"></param>
        /// <returns></returns>
        public static string AccountWithHistoryToString(this Account account)
        {
            if (account == null)
            {
                return string.Empty;
            }

            StringBuilder result = new StringBuilder();
            result.Append(account.ToString());
            if (account.Histories.Count > 0)
            {
                result.Append("\n");
                for (int i = 0; i < account.Histories.Count; i++)
                {
                    result.Append($" {i + 1}. {account.Histories[i]}\n");
                }
            }
            return result.ToString();
        }

        /// <summary>
        /// Строковое представление истории пользователя с указанием пользователя
        /// </summary>
        /// <param name="history"></param>
        /// <returns></returns>
        public static string HistoryWithCustomerToString(this History history)
        {
            if (history.Account?.Customer == null)
            {
                throw new ArgumentNullException(nameof(Customer));
            }
            return $"Клиент: {history.Account.Customer.FullName}. {history}";
        }
    }
}
