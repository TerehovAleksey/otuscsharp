﻿using System;

namespace Lesson6.Domain
{
    /// <summary>
    /// История операций
    /// </summary>
    public class History
    {
        /// <summary>
        /// Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Дата оперции
        /// </summary>
        public DateTime DateTime { get; set; }

        /// <summary>
        /// Тип операции (false-снятие, true-пополнение)
        /// </summary>
        public bool MoneyOperation { get; set; }

        /// <summary>
        /// Сумма
        /// </summary>
        public decimal Money { get; set; }

        #region связь со счётом

        /// <summary>
        /// ID счёта
        /// </summary>
        public int AccountId { get; set; }

        /// <summary>
        /// Счёт
        /// </summary>
        public Account Account { get; set; }

        #endregion

        public override string ToString() => 
            MoneyOperation ? $"Пополнение счёта {DateTime.ToShortDateString()} на сумму: {Money}" : $"Снятие со счёта {DateTime.ToShortDateString()} суммы: {Money}";
    }
}
