﻿using Lesson6.Domain;
using Microsoft.EntityFrameworkCore;
using System;

namespace Lesson6.Context
{
    public class Lesson6DbContext : DbContext
    {
        /// <summary>
        /// Пользователи
        /// </summary>
        public DbSet<Customer> Customers { get; set; }

        /// <summary>
        /// Счета
        /// </summary>
        public DbSet<Account> Accounts { get; set; }

        /// <summary>
        /// Истории операций
        /// </summary>
        public DbSet<History> Histories { get; set; }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="options"></param>
        public Lesson6DbContext()
        {
            if (Database.EnsureCreated())
            {
                Customer customer = Customers.Add(new Customer { Id = 1, LastName = "Иванов", FirstName = "Андрей", Patronomic = "Николаевич", RegisterDate = new DateTime(2018, 10, 02), Phone = "123-45-67", Passport = "IM642GD849", Password = "1", Login = "1" }).Entity;
                Account account1 = Accounts.Add(new Account { Id = 1, CustomerId = customer.Id, Balance = 1000, CreationDate = new DateTime(2018, 10, 2) }).Entity;
                Account account2 = Accounts.Add(new Account { Id = 2, CustomerId = customer.Id, Balance = 10000, CreationDate = new DateTime(2019, 3, 12) }).Entity;
                Histories.Add(new History { Id = 1, AccountId = account1.Id, DateTime = new DateTime(2018, 10, 2), Money = 100, MoneyOperation = true });
                Histories.Add(new History { Id = 2, AccountId = account1.Id, DateTime = new DateTime(2018, 11, 14), Money = 1000, MoneyOperation = false });
                Histories.Add(new History { Id = 3, AccountId = account1.Id, DateTime = new DateTime(2018, 12, 1), Money = 80, MoneyOperation = true });
                Histories.Add(new History { Id = 4, AccountId = account2.Id, DateTime = new DateTime(2019, 3, 13), Money = 20, MoneyOperation = false });
                Histories.Add(new History { Id = 5, AccountId = account2.Id, DateTime = new DateTime(2019, 3, 21), Money = 1000, MoneyOperation = false });
                Histories.Add(new History { Id = 6, AccountId = account2.Id, DateTime = new DateTime(2019, 3, 30), Money = 300, MoneyOperation = true });

                customer = Customers.Add(new Customer { Id = 2, LastName = "Смирнов", FirstName = "Александр", Patronomic = "Петрович", RegisterDate = new DateTime(2019, 05, 22), Phone = "890-12-34", Passport = "JFHSL6472HJ", Password = "2", Login = "2" }).Entity;
                account1 = Accounts.Add(new Account { Id = 3, CustomerId = customer.Id, Balance = 200, CreationDate = new DateTime(2019, 05, 22) }).Entity;
                account2 = Accounts.Add(new Account { Id = 4, CustomerId = customer.Id, Balance = 500, CreationDate = new DateTime(2019, 11, 12) }).Entity;
                Histories.Add(new History { Id = 7, AccountId = account1.Id, DateTime = new DateTime(2019, 05, 23), Money = 500, MoneyOperation = true });
                Histories.Add(new History { Id = 8, AccountId = account1.Id, DateTime = new DateTime(2019, 06, 1), Money = 600, MoneyOperation = false });
                Histories.Add(new History { Id = 9, AccountId = account1.Id, DateTime = new DateTime(2019, 07, 11), Money = 50, MoneyOperation = false });
                Histories.Add(new History { Id = 10, AccountId = account2.Id, DateTime = new DateTime(2019, 08, 3), Money = 1000, MoneyOperation = true });
                Histories.Add(new History { Id = 11, AccountId = account2.Id, DateTime = new DateTime(2019, 09, 2), Money = 5000, MoneyOperation = false });
                Histories.Add(new History { Id = 12, AccountId = account2.Id, DateTime = new DateTime(2019, 10, 17), Money = 800, MoneyOperation = false });

                customer = Customers.Add(new Customer { Id = 3, LastName = "Петров", FirstName = "Николай", Patronomic = "Семёнович", RegisterDate = new DateTime(2018, 02, 15), Phone = "890-12-34", Passport = "JF733LKFS", Password = "3", Login = "3" }).Entity;
                account1 = Accounts.Add(new Account { Id = 5, CustomerId = customer.Id, Balance = 20000, CreationDate = new DateTime(2019, 05, 22) }).Entity;
                Histories.Add(new History { Id = 13, AccountId = account1.Id, DateTime = new DateTime(2018, 03, 19), Money = 8000, MoneyOperation = true });
                Histories.Add(new History { Id = 14, AccountId = account1.Id, DateTime = new DateTime(2018, 04, 11), Money = 3000, MoneyOperation = true });
                Histories.Add(new History { Id = 15, AccountId = account1.Id, DateTime = new DateTime(2018, 06, 17), Money = 10000, MoneyOperation = true });
                Histories.Add(new History { Id = 16, AccountId = account1.Id, DateTime = new DateTime(2018, 09, 30), Money = 1000, MoneyOperation = false });


                SaveChanges();
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Customer>().HasKey(c => c.Id);
            modelBuilder.Entity<Account>().HasKey(a => a.Id);
            modelBuilder.Entity<Account>().Property(a => a.Balance).HasColumnType("NUMERIC");
            modelBuilder.Entity<History>().HasKey(h => h.Id);
            modelBuilder.Entity<History>().Property(h => h.Money).HasColumnType("NUMERIC");
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) => optionsBuilder.UseSqlite("Data Source=database.db");
    }
}
