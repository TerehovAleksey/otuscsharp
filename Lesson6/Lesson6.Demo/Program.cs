﻿using Lesson6.Context;
using Lesson6.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace Lesson6.Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("1. Вывод информации о пользователе по логину и паролю:");
            GetCustomerByIdAndPassword("1", "1");
            Console.WriteLine();

            Console.WriteLine("2. Вывод данных о всех счетах пользователя");
            GetCustomerAccounts(2);
            Console.WriteLine();

            Console.WriteLine("3. Вывод данных о всех счетах пользователя, включая историю по каждому счёту");
            GetCustomerAccountsWithHistory(2);
            Console.WriteLine();

            Console.WriteLine("4. Вывод данных о всех операциях пополнения с указанием владельца каждого счёта");
            GetRefillHistory();
            Console.WriteLine();

            Console.WriteLine("5. Вывод данных о всех пользователях у которых на счёте сумма больше заданной");
            GetCustomersWithAccontGreaterThan(10000);

            Console.ReadKey();
        }

        /// <summary>
        /// Вывод информации о пользователе по логину и паролю
        /// </summary>
        /// <param name="login">логин</param>
        /// <param name="password">пароль</param>
        private static void GetCustomerByIdAndPassword(string login, string password)
        {
            using (Lesson6DbContext db = new Lesson6DbContext())
            {
                var customer = db.Customers
                    .Where(c => c.Login.Equals(login) && c.Password.Equals(password))
                    .AsNoTracking()
                    .FirstOrDefault();
                Console.WriteLine(customer);
            }
        }

        /// <summary>
        /// Вывод данных о всех счетах пользователя
        /// </summary>
        /// <param name="customerId">Id пользователя</param>
        private static void GetCustomerAccounts(int customerId)
        {
            using (Lesson6DbContext db = new Lesson6DbContext())
            {
                var accounts = db.Accounts
                    .Include(a => a.Customer)
                    .Where(a => a.CustomerId == customerId)
                    .AsNoTracking()
                    .AsEnumerable();
                foreach (var account in accounts)
                {
                    Console.WriteLine(account.ToString());
                }
            }
        }

        /// <summary>
        /// Вывод данных о всех счетах пользователя, включая историю по каждому счёту
        /// </summary>
        /// <param name="customerId">Id пользователя</param>
        private static void GetCustomerAccountsWithHistory(int customerId)
        {
            using (Lesson6DbContext db = new Lesson6DbContext())
            {
                var accounts = db.Accounts
                    .Include(a => a.Customer)
                    .Include(a => a.Histories)
                    .Where(a => a.CustomerId == customerId)
                    .AsNoTracking()
                    .AsEnumerable();
                foreach (var account in accounts)
                {
                    Console.WriteLine(account.AccountWithHistoryToString());
                }
            }
        }

        /// <summary>
        /// Вывод данных о всех операциях пополнения с указанием владельца каждого счёта
        /// </summary>
        private static void GetRefillHistory()
        {
            using (Lesson6DbContext db = new Lesson6DbContext())
            {
                var histories = db.Histories
                    .Include(h => h.Account.Customer)
                    .Where(h => h.MoneyOperation)
                    .OrderBy(h => h.DateTime)
                    .AsNoTracking()
                    .AsEnumerable();
                foreach (var history in histories)
                {
                    Console.WriteLine(history.HistoryWithCustomerToString());
                }
            }
        }

        /// <summary>
        /// Вывод данных о всех пользователях у которых на счёте сумма больше заданной
        /// </summary>
        /// <param name="money">сумма</param>
        private static void GetCustomersWithAccontGreaterThan(decimal money)
        {
            using (Lesson6DbContext db = new Lesson6DbContext())
            {
                // EF не может транслировать в sqlite предикат (a) => (a.Balance > money), сделано в два действия
                var user = db.Customers
                    .Include(c => c.Accounts)
                    .AsEnumerable();
                user = user.Where(u => u.Accounts.Any(a => a.Balance > money));
                foreach (var item in user)
                {
                    Console.WriteLine(item);
                }
            }
        }
    }
}
