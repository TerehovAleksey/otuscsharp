﻿namespace Lesson4
{
    /// <summary>
    /// Модель
    /// </summary>
    public class Model : BaseEntity
    {
        /// <summary>
        /// Производитель
        /// </summary>
        public string Manufacturer { get; set; }

        public override string ToString() => $"{Manufacturer} {Name}";
    }
}
