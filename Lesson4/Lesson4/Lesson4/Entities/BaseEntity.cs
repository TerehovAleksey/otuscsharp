﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace Lesson4
{
    /// <summary>
    /// Базовый класс для сущностей
    /// </summary>
    public abstract class BaseEntity : IEquatable<BaseEntity>
    {
        /// <summary>
        /// Уникальный ID
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Название
        /// </summary>
        public string Name { get; set; }

        #region Equals and ToString

        public bool Equals([AllowNull] BaseEntity other)
        {
            return Id == other.Id && Name == other.Name;
        }

        public override bool Equals(object obj)
        {
            if (obj is BaseEntity entity)
            {
                return Equals(entity);
            }
            return false;
        }

        public override int GetHashCode() => HashCode.Combine(Id, Name);

        public override string ToString() => Name;

        #endregion
    }
}
