﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lesson4
{
    public class ManufacturerFakeData : IManufacturerData
    {
        private static readonly List<Manufacturer> manufacturers;

        static ManufacturerFakeData()
        {
            manufacturers = new List<Manufacturer>
            {
                new Manufacturer{ Id = 1, Name = "Huawei" },
                new Manufacturer{ Id = 2, Name = "Samsung" },
                new Manufacturer{ Id = 3, Name = "Xiaomi" },
            };
        }

        public void Add(Manufacturer item)
        {
            manufacturers.Add(item);
        }

        public void Delete(int id)
        {
            var item = manufacturers.FirstOrDefault(i => i.Id == id);
            if (item != null)
            {
                manufacturers.Remove(item);
            }
        }

        public Manufacturer Get(int id)
        {
            return manufacturers.FirstOrDefault(i => i.Id == id);
        }

        public void Update(Manufacturer item)
        {
            var updated = manufacturers.FirstOrDefault(i => i.Id == item.Id);
            if (updated != null)
            {
                manufacturers.Remove(updated);
                manufacturers.Add(item);
            }
        }
    }
}
