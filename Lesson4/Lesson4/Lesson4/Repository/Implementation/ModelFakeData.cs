﻿using System.Collections.Generic;
using System.Linq;

namespace Lesson4
{
    public class ModelFakeData : IModelData
    {
        private static readonly List<Model> models;

        static ModelFakeData()
        {
            models = new List<Model>
            {
                new Model{ Id = 1, Manufacturer = "Huawei", Name = "P30"},
                new Model{ Id = 2, Manufacturer = "Samsung", Name = "S10"},
                new Model{ Id = 3, Manufacturer = "Xiaomi", Name = "Note 8"}
            };
        }

        public void Add(Model item)
        {
            models.Add(item);
        }

        public void Delete(int id)
        {
            var item = models.FirstOrDefault(i => i.Id == id);
            if (item != null)
            {
                models.Remove(item);
            }
        }

        public Model Get(int id)
        {
            return models.FirstOrDefault(i => i.Id == id);
        }

        public void Update(Model item)
        {
            var updated = models.FirstOrDefault(i => i.Id == item.Id);
            if (updated != null)
            {
                models.Remove(updated);
                models.Add(item);
            }
        }
    }
}
