﻿namespace Lesson4
{
    public class Repository : IRepository
    {
        /// <summary>
        /// Производители
        /// </summary>
        public IManufacturerData Manufacturers => new ManufacturerFakeData();

        /// <summary>
        /// Модели
        /// </summary>
        public IModelData Models => new ModelFakeData();
    }
}
