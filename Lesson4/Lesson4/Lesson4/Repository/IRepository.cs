﻿namespace Lesson4
{
    /// <summary>
    /// Репозиторий
    /// </summary>
    public interface IRepository
    {
        /// <summary>
        /// Производители
        /// </summary>
        public IManufacturerData Manufacturers { get; }

        /// <summary>
        /// Модели
        /// </summary>
        public IModelData Models { get; }
    }
}
