﻿namespace Lesson4
{
    /// <summary>
    /// Интерфейс работы с производителями
    /// </summary>
    public interface IManufacturerData : ICrudData<Manufacturer>
    {

    }
}
