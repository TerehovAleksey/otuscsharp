﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lesson4
{
    /// <summary>
    /// Интерфейс работы с моделями
    /// </summary>
    public interface IModelData : ICrudData<Model>
    {
    }
}
