﻿namespace Lesson4
{
    /// <summary>
    /// Базовый интерфейс для работы с сущностями 
    /// </summary>
    public interface ICrudData<T> where T : BaseEntity
    {
        /// <summary>
        /// Добавление элемента
        /// </summary>
        void Add(T item);

        /// <summary>
        /// Получение элемента по ID
        /// </summary>
        T Get(int id);

        /// <summary>
        /// Обновление элемента
        /// </summary>
        void Update(T item);

        /// <summary>
        /// Удаление элемента
        /// </summary>
        void Delete(int id);
    }
}
