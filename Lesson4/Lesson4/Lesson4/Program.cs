﻿using System;

namespace Lesson4
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Использование интефейсов и абстрактных классов на примере паттерна \"репозиторий\"");
            Console.WriteLine(new string('-',40));
            Console.WriteLine();
            Console.WriteLine("Проверка работы");
            Console.WriteLine();

            IRepository repository = new Repository();

            var manufacturer = repository.Manufacturers.Get(1);
            Console.WriteLine($"Получение производителя по ID: {manufacturer}");
            Console.WriteLine();

            repository.Manufacturers.Add(new Manufacturer { Id = 4, Name = "Apple" });
            Console.WriteLine($"Добавлен производитель: {repository.Manufacturers.Get(4)}");
            Console.WriteLine();

            var model = repository.Models.Get(2);
            Console.WriteLine($"Получение модели по ID: {model}");
            Console.WriteLine("Удаление этой модели");
            repository.Models.Delete(2);
            model = repository.Models.Get(2);
            Console.WriteLine($"Проверка: {model?.ToString() ?? "модель не найдена"}");

            Console.ReadLine();
        }
    }
}
