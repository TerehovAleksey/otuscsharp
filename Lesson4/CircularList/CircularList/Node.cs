﻿using System;

namespace CircularList
{
    public class Node<T>
    {
        private T value = default;

        public T Value
        {
            get => value;
            set => this.value = value ?? throw new ArgumentNullException(nameof(value));
        }
        public Node<T> Next { get; set; }
        public Node<T> Previous { get; set; }

        public Node(T value)
        {
            Value = value;
        }

        public override string ToString() => Value.ToString();
    }
}
