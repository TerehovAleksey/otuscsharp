﻿using System.Collections;

namespace CircularList
{
    public class CurcularList<T> : IEnumerable
    {
        // Первый элемент списка
        private Node<T> first;

        /// <summary>
        /// Количество элементов
        /// </summary>
        public int Count { get; private set; }

        /// <summary>
        /// Пустой ли список
        /// </summary>
        public bool IsEmpty => Count == 0;

        /// <summary>
        /// Контсруктор
        /// </summary>
        public CurcularList()
        {
            Clear();
        }

        /// <summary>
        /// Контсруктор
        /// </summary>
        public CurcularList(T value)
        {
            var node = new Node<T>(value);
            InitList(node);
        }

        /// <summary>
        /// Очистка списка
        /// </summary>
        public void Clear()
        {
            first = null;
            Count = 0;
        }

        /// <summary>
        /// Добавление элемента
        /// </summary>
        /// <param name="value"></param>
        public void Add(T value)
        {
            var node = new Node<T>(value);
            if (first == null)
            {
                InitList(node);
            }
            else
            {
                node.Previous = first.Previous;
                node.Next = first;
                first.Previous.Next = node;
                first.Previous = node;

                Count++;
            }
        }

        /// <summary>
        /// Удаление элемента
        /// </summary>
        /// <param name="value"></param>
        public void Remove(T value)
        {
            if (Count == 0)
            {
                return;
            }

            var current = first;
            Node<T> deleted = null;

            do
            {
                if (current.Value.Equals(value))
                {
                    deleted = current;
                    break;
                }
                current = current.Next;
            } while (current != first);

            if (deleted != null)
            {
                if (Count == 1)
                {
                    Clear();
                }
                else
                {
                    if (deleted == first)
                    {
                        first = first.Next;
                    }
                    deleted.Previous.Next = deleted.Next;
                    deleted.Next.Previous = deleted.Previous;
                    Count--;
                }
            }
        }

        // инициализация списка одним элементом
        private void InitList(Node<T> node)
        {
            node.Next = node;
            node.Previous = node;
            first = node;
            Count++;
        }

        public IEnumerator GetEnumerator()
        {
            var current = first;

            // первый цикл
            int circle = 1;

            while (current != null && circle == 1)
            {
                yield return current.Value;
                current = current.Next;

                if (current == first)
                {
                    // ушли на вторй цикл
                    circle = 2;
                }
            }
        }
    }
}
