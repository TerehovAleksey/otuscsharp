﻿using CircularList;
using System;

namespace CircularListDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            CurcularList<int> circularList = new CurcularList<int>();

            Console.WriteLine("добавляем элементы в список");
            circularList.Print();
            circularList.Add(1);
            circularList.Print();
            circularList.Add(2);
            circularList.Print();
            circularList.Add(3);
            circularList.Print();
            circularList.Add(4);
            circularList.Print();

            Console.WriteLine("удаляем 2");
            circularList.Remove(2);
            circularList.Print();
            Console.WriteLine("удаляем 1");
            circularList.Remove(1);
            circularList.Print();
            Console.WriteLine("удаляем 8");
            circularList.Remove(2);
            circularList.Print();

            Console.WriteLine();


            Console.ReadKey();
        }
    }
}
