﻿using System;
using System.Collections.Generic;
using System.Text;
using CircularList;

namespace CircularListDemo
{
    public static class CircularListExtensions
    {
        public static void Print(this CurcularList<int> circularList)
        {
            foreach (var item in circularList)
            {
                Console.Write($"{item} ");
            }
            Console.Write($"  - Количество: {circularList.Count}");
            Console.WriteLine();
        }
    }
}
