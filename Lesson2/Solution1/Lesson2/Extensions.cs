﻿using System;

namespace Lesson2
{
    public static class Extensions
    {
        /// <summary>
        /// Получение возраста сотрудника (полных лет)
        /// </summary>
        /// <param name="employee"></param>
        /// <returns></returns>
        public static int GetAge(this Employee employee)
        {
            if (employee.Birsday == default)
            {
                return 0;
            }

            DateTime now = DateTime.Now;
            int age = now.Year - employee.Birsday.Year;
            if (now.Month < employee.Birsday.Month || (now.Month == employee.Birsday.Month && now.Day < employee.Birsday.Day))
            {
                age--;
            }

            return age;
        }
    }
}
