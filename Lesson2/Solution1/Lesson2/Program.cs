﻿using System;

namespace Lesson2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Программа проверки методов и операторов");
            Console.WriteLine(new string('-', 40));
            Console.WriteLine();

            Employee employee1 = new Employee("Сергей", "Иванов", new DateTime(2012, 03, 26))
            {
                Birsday = new DateTime(1990, 03, 12), 
            };
            Console.WriteLine("1-й сотрудник (конструктор 1):");
            Console.WriteLine(employee1);
            Console.WriteLine();

            Employee employee2 = new Employee("Николай", "Смирнов", new DateTime(2001, 01, 06), new DateTime(2012, 12, 20));
            Console.WriteLine("2-й сотрудник (конструктор 2):");
            Console.WriteLine(employee2);
            Console.WriteLine();

            Employee employee3 = new Employee("Сергей", "Иванов", new DateTime(1990, 03, 12), new DateTime(2012, 03, 26));
            Console.WriteLine("3-й сотрудник (для проверки работы операторов сравнения):");
            Console.WriteLine(employee3);
            Console.WriteLine();

            Console.WriteLine("Работа метода :");
            Console.WriteLine($"{employee1} стаж полных лет: {employee1.GetYearsOfWork()}");
            Console.WriteLine($"{employee2} стаж полных лет: {employee2.GetYearsOfWork()}");
            Console.WriteLine();

            Console.WriteLine("Работа метода расширения:");
            Console.WriteLine($"Возраст {employee1}: {employee1.GetAge()}");
            Console.WriteLine($"Возраст {employee2}: {employee2.GetAge()}");
            Console.WriteLine();

            Console.WriteLine("Работа сравнений:");
            Console.WriteLine($"Equals {employee1} и {employee2} = {employee1.Equals(employee2)}");
            Console.WriteLine($"Equals {employee1} и {employee3} = {employee1.Equals(employee3)}");
            Console.WriteLine($"{employee1} != {employee2} = {employee1 == employee2}");
            Console.WriteLine($"{employee1} == {employee3} = {employee1 == employee3}");       
            Console.WriteLine($"ReferenceEquals {employee1} и {employee3} = {ReferenceEquals(employee1, employee3)}");
            Console.WriteLine($"{employee1} > {employee2} = {employee1 > employee2}");
            Console.WriteLine($"{employee1} < {employee2} = {employee1 < employee2}");
            Console.WriteLine();

            Console.WriteLine("Работа индексатора:");
            employee1.Id = 1;
            employee2.Id = 2;
            employee3.Id = 3;
            EmployeeManager manager = new EmployeeManager();
            manager.AddEmployee(employee1);
            manager.AddEmployee(employee2);
            manager.AddEmployee(employee3);
            Console.WriteLine(manager[2]);
            Console.WriteLine();

            Console.WriteLine("Ну и просто ошибка при передаче неверного аргумента в конструктор:");
            try
            {
                var emp4 = new Employee("Виктор", "", default);
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine(ex);
            }

            Console.WriteLine();
            Console.WriteLine(new string('-', 40));
            Console.WriteLine("На выполнение задания ушло около часа");
            Console.WriteLine(new string('-', 40));

            Console.ReadLine();
        }
    }
}
