﻿using System;

namespace Lesson2
{
    /// <summary>
    /// Сотрудник
    /// </summary>
    public class Employee
    {
        #region Private Members

        private string firstName;
        private string lastName;

        //дата начала работы
        private readonly DateTime startWorkDate;

        #endregion

        #region Public Properties

        /// <summary>
        /// ID сотрудника
        /// </summary>
        public int Id { get; set; } = default;

        /// <summary>
        /// Имя сотрудника
        /// </summary>
        public string FirstName
        {
            get => firstName;
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    throw new ArgumentException(nameof(FirstName));
                }
                firstName = value;
            }
        }

        /// <summary>
        /// Фамилия сотрудника
        /// </summary>
        public string LastName
        {
            get => lastName;
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    throw new ArgumentException(nameof(LastName));
                }
                lastName = value;
            }
        }

        /// <summary>
        /// День рождения сотрудника
        /// </summary>
        public DateTime Birsday { get; set; } = default;

        #endregion

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="firstName">Имя сотрудника</param>
        /// <param name="lastName">Фамилия сотрудника</param>
        /// <param name="birsday">День рождения</param>
        /// <param name="startWorkDate">Дата начала работы</param>
        /// <exception cref="ArgumentException"></exception>
        public Employee(string firstName, string lastName, DateTime birsday, DateTime startWorkDate)
        {
            FirstName = firstName;
            LastName = lastName;
            Birsday = birsday;
            this.startWorkDate = startWorkDate;
        }

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="firstName">Имя сотрудника</param>
        /// <param name="lastName">Фамилия сотрудника</param>
        /// <param name="startWorkDate">Дата начала работы</param>
        /// <exception cref="ArgumentException"></exception>
        public Employee(string firstName, string lastName, DateTime startWorkDate) : this(firstName, lastName, default, startWorkDate)
        {

        }

        #region Equals and ToString

        public override bool Equals(object obj)
        {
            if (obj is Employee employee)
            {
                return employee.Id == Id &&
                    employee.FirstName == FirstName &&
                    employee.LastName == LastName &&
                    employee.Birsday == Birsday &&
                    employee.startWorkDate == startWorkDate;
            }
            else
            {
                return false;
            }
        }

        public override int GetHashCode() => HashCode.Combine(FirstName, LastName, Birsday);

        public override string ToString() => $"{FirstName} {LastName}".Trim();

        #endregion

        #region Перегрузка операторов

        public static bool operator ==(Employee employee1, Employee employee2) => employee1.Equals(employee2);
        public static bool operator !=(Employee employee1, Employee employee2) => !employee1.Equals(employee2);

        /// <summary>
        /// Сравнение по возрасту сотрудников
        /// </summary>
        /// <returns></returns>
        public static bool operator >(Employee employee1, Employee employee2) => employee1.Birsday < employee2.Birsday;

        /// <summary>
        /// Сравнение по возрасту сотрудников
        /// </summary>
        /// <returns></returns>
        public static bool operator <(Employee employee1, Employee employee2) => employee1.Birsday > employee2.Birsday;

        #endregion

        #region Методы

        /// <summary>
        /// Получение стажа работы (полных лет)
        /// </summary>
        /// <returns></returns>
        public int GetYearsOfWork()
        {
            return DateTime.Now.Subtract(startWorkDate).Days / 365;
        }

        #endregion
    }
}
