﻿using System.Collections.Generic;
using System.Linq;

namespace Lesson2
{
    /// <summary>
    /// Класс управления сотрудниками
    /// </summary>
    public class EmployeeManager
    {
        private readonly List<Employee> employees = new List<Employee>();

        /// <summary>
        /// Добавление сотрудника
        /// </summary>
        /// <param name="employee"></param>
        public void AddEmployee(Employee employee)
        {
            employees.Add(employee);
        }

        /// <summary>
        /// Индексатор
        /// </summary>
        /// <param name="id">ID сотрудника</param>
        /// <returns></returns>
        public Employee this[int id]
        {
            get => employees.FirstOrDefault(e => e.Id == id);
        }
    }
}
